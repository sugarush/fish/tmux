set -g tmux_module_conf $sugar_install_directory/config/tmux.fish

if test ! -f $tmux_module_conf
  cp $argv[1]/config/tmux.fish $tmux_module_conf
end

set -g tmux_conf $argv[1]/conf.d

function tmux-has-session
  set -l session $argv[1]
  tmux has-session -t $session > /dev/null 2>&1
end

function tmux-configure
  source $tmux_module_conf

  tmux source-file $tmux_conf/primary
  tmux source-file $tmux_conf/keys

  if $tmux_status_bar_top
    tmux set-option -g status-position top
  else
    tmux set-option -g status-position bottom
  end

  if $tmux_enable_mouse
    tmux set-option -g mouse on
    tmux source-file $tmux_conf/mouse
    if test (uname) = "Darwin"
      tmux source-file $tmux_conf/mouse-osx
    else
      tmux source-file $tmux_conf/mouse-linux
    end
  else
    tmux set-option -g mouse off
  end

  tmux source-file $tmux_conf/theme

  if $tmux_light_theme
    tmux source-file $tmux_conf/theme-light
  else
    tmux source-file $tmux_conf/theme-dark
  end
end

function tmux-session
  set -l session $argv[1]

  if test -z $session
    set session default
  end

  if ! tmux-has-session $session
    tmux new-session -d -c $HOME -s $session > /dev/null
    tmux-configure
  end

  if test -z $TMUX
    tmux attach-session -t $session
  else
    tmux switch-client -t $session
  end
end
